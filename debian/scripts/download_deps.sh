#!/bin/bash

set -e

TAG=ndk-r26d
DL_DIR=debian/download-upstream

mkdir -p "$DL_DIR"
cd "$DL_DIR"
repo init -u https://android.googlesource.com/platform/manifest -b "$TAG" --depth=1 -p linux

# Remove unrequired repos
cp -a .repo/manifests/default.xml .repo/manifests/default.bak
sed -i \
  -e "/darwin-x86/d" \
  -e "/windows-x86/d" \
  -e "/mingw32/d" \
  -e "/prebuilts\/python/d" \
  -e "/prebuilts\/build-tools/d" \
  -e "/prebuilts\/ninja/d" \
  -e "/prebuilts\/cmake/d" \
  -e "/toolchain\/make/d" \
  -e "/toolchain\/yasm/d" \
  .repo/manifests/default.xml

# Download
repo sync

# Remove unrequired dirs for clang
CLANG_VERSION="$(grep "CLANG_VERSION = " ndk/ndk/toolchains.py  | cut -d '"' -f 2)"
if [ -n "$CLANG_VERSION" ]; then
  find prebuilts/clang/host/linux-x86/ -mindepth 1 -maxdepth 1 -not -name "$CLANG_VERSION" -and -not -name ".git" -exec rm -fr "{}" \;
fi

# Remove unrequired dirs for development
find development/ -mindepth 1 -maxdepth 1 -not -name python-packages -and -not -name ".git" -exec rm -fr "{}" \;

# Remove unrequired dirs for development
find prebuilts/ndk/ -mindepth 1 -maxdepth 1 -not -name platform -and -not -name ".git" -exec rm -fr "{}" \;

# Rename ndk & development/python-packages
mv ndk ndk.disabled
mv development/python-packages development/python-packages.disabled
